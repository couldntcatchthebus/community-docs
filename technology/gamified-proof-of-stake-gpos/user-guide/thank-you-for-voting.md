# Thank you for voting!

You made it this far; now you qualify for participation rewards for contributing to the governance of Peerplays.

After you vote you'll see the following screen:

![](<../../../.gitbook/assets/Screen Shot 2020-02-12 at 4.08.08 PM.png>)

So like the avatar says, go and share your experience with your friends, family and the Peerplays community.
