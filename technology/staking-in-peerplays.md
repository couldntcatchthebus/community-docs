# Staking (PowerUp) in Peerplays

![](../.gitbook/assets/NFT-Staking.png)

## Downloads

The Diagram in PDF form to easily view and share:

{% file src="../.gitbook/assets/NFT-Staking.pdf" %}
Staking (PowerUp) in Peerplays - PDF
{% endfile %}

The Diagram in Draw.io's XML format. You can edit the diargam using this file in Draw.io:

{% file src="../.gitbook/assets/NFT-Staking.xml" %}
Staking (PowerUp) in Peerplays - XML
{% endfile %}
